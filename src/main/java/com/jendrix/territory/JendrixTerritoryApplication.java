package com.jendrix.territory;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JendrixTerritoryApplication {

	public static void main(String[] args) {
		SpringApplication.run(JendrixTerritoryApplication.class, args);
	}

}
