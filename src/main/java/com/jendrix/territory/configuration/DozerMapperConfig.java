package com.jendrix.territory.configuration;

import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DozerMapperConfig {

	@Bean // (2)
	public Mapper beanMapper() {
		return new DozerBeanMapper(); // (3)
	}
}
