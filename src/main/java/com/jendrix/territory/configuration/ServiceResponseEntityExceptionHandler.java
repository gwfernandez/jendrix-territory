package com.jendrix.territory.configuration;

import java.time.LocalDateTime;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.jendrix.common.exception.ServiceException;
import com.jendrix.common.exception.ServiceRestrictionException;

@ControllerAdvice
public class ServiceResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

	@ExceptionHandler(ServiceRestrictionException.class)
	public ResponseEntity<CustomErrorResponse> handleServiceRestrictionException(ServiceRestrictionException ex,
			WebRequest request) {

		CustomErrorResponse msg = new CustomErrorResponse();
		msg.setTimestamp(LocalDateTime.now());
		msg.setStatus(HttpStatus.BAD_REQUEST.value());
		//msg.setError(ex.getMessage());
		msg.setMessages(ex.getMessages());

		return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(ServiceException.class)
	public ResponseEntity<CustomErrorResponse> handleServiceException(ServiceException ex, WebRequest request) {

		CustomErrorResponse msg = new CustomErrorResponse();
		msg.setTimestamp(LocalDateTime.now());
		msg.setStatus(HttpStatus.BAD_REQUEST.value());
		msg.setError(ex.getMessage());

		return new ResponseEntity<>(msg, HttpStatus.BAD_REQUEST);

	}

}