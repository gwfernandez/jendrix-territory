package com.jendrix.territory.consumer;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GeonamesConsumer {

	private static final Log log = LogFactory.getLog(GeonamesConsumer.class);
	private static final String URL_SERVICE_GEONAMES = "http://www.geonames.org/childrenJSON?geonameId=%s&lang=es";

	public static JsonNode request(Long geonameId) throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		String urlService = String.format(URL_SERVICE_GEONAMES, geonameId);
		log.info("rest url: " + urlService);
		ResponseEntity<String> response = restTemplate.getForEntity(urlService, String.class);

		JsonNode result = null;
		if (response.getStatusCodeValue() == HttpStatus.OK.value()) {
			log.info("rest service status OK");
			ObjectMapper mapper = new ObjectMapper();
			JsonNode root = mapper.readTree(response.getBody());
			result = root.findValue("geonames");
		}

		return result;
	}
}
