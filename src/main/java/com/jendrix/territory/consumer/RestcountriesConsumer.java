package com.jendrix.territory.consumer;

import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RestcountriesConsumer {

	private static final Log log = LogFactory.getLog(RestcountriesConsumer.class);
	private static final String URL_SERVICE_RESTCOUNTRIES = "https://restcountries.eu/rest/v2/region/%s";

	public static JsonNode request(String continente) throws IOException {
		RestTemplate restTemplate = new RestTemplate();
		String urlService = String.format(URL_SERVICE_RESTCOUNTRIES, continente);
		log.info("rest url: " + urlService);
		ResponseEntity<String> response = restTemplate.getForEntity(urlService, String.class);

		JsonNode result = null;
		if (response.getStatusCodeValue() == HttpStatus.OK.value()) {
			ObjectMapper mapper = new ObjectMapper();
			result = mapper.readTree(response.getBody());
		}
		return result;

	}
}
