package com.jendrix.territory.controller.response;

import java.io.Serializable;

public class ProcessStatusResponse implements Serializable {

	private static final long serialVersionUID = 4821854262243636474L;

	private String status;
	private String message;
	private String detail;

	private ProcessStatusResponse(String status, String message, String detail) {
		this.status = status;
		this.message = message;
		this.detail = detail;
	}

	private ProcessStatusResponse(String status, String message) {
		this(status, message, null);
	}

	public static ProcessStatusResponse success(String message, String detail) {
		return new ProcessStatusResponse(ProcessStatusType.SUCCESS.getName(), message, detail);
	}

	public static ProcessStatusResponse success(String message) {
		return new ProcessStatusResponse(ProcessStatusType.SUCCESS.getName(), message);
	}

	public static ProcessStatusResponse error(String message, String detail) {
		return new ProcessStatusResponse(ProcessStatusType.ERROR.getName(), message, detail);
	}

	public static ProcessStatusResponse error(String message) {
		return new ProcessStatusResponse(ProcessStatusType.ERROR.getName(), message);
	}

	public String getStatus() {
		return status;
	}

	public String getMessage() {
		return message;
	}

	public String getDetail() {
		return detail;
	}
}
