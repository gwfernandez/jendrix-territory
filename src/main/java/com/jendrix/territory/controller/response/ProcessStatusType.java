package com.jendrix.territory.controller.response;

public enum ProcessStatusType {

	SUCCESS("success"), ERROR("error");

	private String name;

	private ProcessStatusType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
}
