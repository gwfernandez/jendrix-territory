package com.jendrix.territory.controller.rest;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.jendrix.territory.model.response.ContinenteResponse;
import com.jendrix.territory.service.ContinenteService;

@RestController
@RequestMapping("/continente")
@Validated
public class ContinenteRestController {

	@Autowired
	@Qualifier("continenteServiceImpl")
	private ContinenteService continenteService;

	@GetMapping(value = "/findAll")
	public ResponseEntity<Iterable<ContinenteResponse>> findAll() {
		return ResponseEntity.ok(continenteService.findAll());
	}

	@GetMapping(value = "/findBy/id/{id}")
	public ResponseEntity<ContinenteResponse> findById(@PathVariable("id") @Positive Long id) {

		ContinenteResponse response = continenteService.findById(id);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro el continente con ID " + id);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/findBy/codigo/{codigo}")
	public ResponseEntity<ContinenteResponse> findByCodigo(
			@PathVariable("codigo") @NotBlank @Size(min = 2, max = 2) String codigo) {

		ContinenteResponse response = continenteService.findByCodigo(codigo);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro el continente " + codigo);
		}

		return ResponseEntity.ok(response);
	}

	@GetMapping(value = "/findBy/nombre/{nombre}")
	public ResponseEntity<Iterable<ContinenteResponse>> findByNombre(
			@PathVariable("nombre") @NotBlank @Size(min = 2, max = 100) String nombre) {
		List<ContinenteResponse> list = continenteService.findByNombre(nombre);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron continentes con el nombre " + nombre);
		}
		return ResponseEntity.ok(list);
	}

}
