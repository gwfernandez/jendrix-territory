package com.jendrix.territory.controller.rest;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.jendrix.territory.model.response.LocalidadResponse;
import com.jendrix.territory.service.LocalidadService;

@RestController
@RequestMapping("/localidad")
@Validated
public class LocalidadRestController {

	@Autowired
	private LocalidadService localidadService;

	@GetMapping(value = "/findBy/id/{id}")
	public LocalidadResponse findById(@PathVariable("id") @Positive Long id) {
		LocalidadResponse response = localidadService.findById(id);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro la localidad con ID " + id);
		}
		return response;
	}

	@GetMapping(value = "/findBy/nombre/{partidoId}/{nombre}")
	public Iterable<LocalidadResponse> findByNombre(@PathVariable("partidoId") @Positive Long partidoId,
			@PathVariable("nombre") @NotBlank @Size(min = 2, max = 100) String nombre) {
		Iterable<LocalidadResponse> list = localidadService.findByNombre(partidoId, nombre);
		if (list == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron localidades con nombre " + nombre + " para el partido ID " + partidoId);
		}
		return list;
	}

	@GetMapping(value = "/findBy/partido/id/{partidoId}")
	public Iterable<LocalidadResponse> findByPartidoId(@PathVariable("partidoId") @Positive Long partidoId) {
		Iterable<LocalidadResponse> list = localidadService.findByPartidoId(partidoId);
		if (list == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron localidades para el partido ID " + partidoId);
		}
		return list;
	}

}
