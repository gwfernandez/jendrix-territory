package com.jendrix.territory.controller.rest;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.jendrix.territory.model.response.PaisResponse;
import com.jendrix.territory.service.PaisService;

@RestController
@RequestMapping("/pais")
@Validated
public class PaisRestController {

	@Autowired
	@Qualifier("paisServiceImpl")
	private PaisService paisService;

	@GetMapping(value = "/findBy/continente/{continente}")
	public Iterable<PaisResponse> findByContinente(
			@PathVariable("continente") @NotBlank @Size(min = 2, max = 2) String continente) {
		List<PaisResponse> list = paisService.findByContinente(continente);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron paises para el continente " + continente);
		}
		return list;
	}

	@GetMapping(value = "/findBy/id/{id}")
	public PaisResponse findById(@PathVariable("id") @Positive Long id) {
		PaisResponse response = paisService.findById(id);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro el pais con ID " + id);
		}
		return response;
	}

	@GetMapping(value = "/findBy/codigoAlfa2/{codigo}")
	public PaisResponse findByCodigoAlfa2(@PathVariable("codigo") @NotBlank @Size(min = 2, max = 2) String codigo) {
		PaisResponse response = paisService.findByCodigoAlfa2(codigo);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro el pais con el codigo " + codigo);
		}
		return response;
	}

	@GetMapping(value = "/findBy/codigoAlfa3/{codigo}")
	public PaisResponse findByCodigoAlfa3(@PathVariable("codigo") @NotBlank @Size(min = 3, max = 3) String codigo) {
		PaisResponse response = paisService.findByCodigoAlfa3(codigo);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro el pais con el codigo " + codigo);
		}
		return response;
	}

	@GetMapping(value = "/findBy/nombre/{nombre}")
	public Iterable<PaisResponse> findByNombre(
			@PathVariable("nombre") @NotBlank @Size(min = 2, max = 100) String nombre) {
		List<PaisResponse> list = paisService.findByNombre(nombre);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontraron paises con el nombre " + nombre);
		}
		return list;
	}

	@GetMapping(value = "/findBy/nombre/startingWith/{letra}")
	public Iterable<PaisResponse> findByNombreStartingWith(
			@PathVariable("letra") @NotBlank @Size(min = 1, max = 1) String letra) {
		List<PaisResponse> list = paisService.findByNombreStartingWith(letra);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron paises que comiencen con la letra " + letra);
		}
		return list;
	}
}
