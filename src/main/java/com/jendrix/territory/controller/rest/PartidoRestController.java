package com.jendrix.territory.controller.rest;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.jendrix.territory.model.response.PartidoResponse;
import com.jendrix.territory.service.PartidoService;

@RestController
@RequestMapping("/partido")
@Validated
public class PartidoRestController {

	@Autowired
	private PartidoService partidoService;

	@GetMapping(value = "/findBy/id/{id}")
	public PartidoResponse findById(@PathVariable("id") @Positive Long id) {
		PartidoResponse response = partidoService.findById(id);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro la partido con ID " + id);
		}
		return response;
	}

	@GetMapping(value = "/findBy/nombre/{provinciaId}/{nombre}")
	public Iterable<PartidoResponse> findByNombre(@PathVariable("provinciaId") @Positive Long provinciaId,
			@PathVariable("nombre") @NotBlank @Size(min = 2, max = 100) String nombre) {
		List<PartidoResponse> list = partidoService.findByNombre(provinciaId, nombre);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron partidos con nombre " + nombre + " para la provincia ID " + provinciaId);
		}
		return list;
	}

	@GetMapping(value = "/findBy/provincia/codigoISO/{codigoISO}")
	public Iterable<PartidoResponse> findByProvincia(
			@PathVariable("codigoISO") @NotBlank @Size(min = 2, max = 15) String codigoISO) {
		List<PartidoResponse> list = partidoService.findByProvincia(codigoISO);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron partidos para la provincia " + codigoISO);
		}
		return list;
	}

	@GetMapping(value = "/findBy/provincia/id/{provinciaId}")
	public Iterable<PartidoResponse> findByProvinciaId(@PathVariable("provinciaId") @Positive Long provinciaId) {
		List<PartidoResponse> list = partidoService.findByProvinciaId(provinciaId);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron partidos para la provincia ID " + provinciaId);
		}
		return list;
	}

}
