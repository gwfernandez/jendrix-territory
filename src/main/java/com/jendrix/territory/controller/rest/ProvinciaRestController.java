package com.jendrix.territory.controller.rest;

import java.util.List;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.jendrix.territory.model.response.ProvinciaResponse;
import com.jendrix.territory.service.ProvinciaService;

@RestController
@RequestMapping("/provincia")
@Validated
public class ProvinciaRestController {

	@Autowired
	private ProvinciaService provinciaService;

	@GetMapping(value = "/findBy/id/{id}")
	public ProvinciaResponse findById(@PathVariable("id") @Positive Long id) {
		ProvinciaResponse response = provinciaService.findById(id);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, "No se encontro la provincia con ID " + id);
		}
		return response;
	}

	@GetMapping(value = "/findBy/codigoISO/{codigoISO}")
	public ProvinciaResponse findByReferencia(
			@PathVariable("codigoISO") @NotBlank @Size(min = 2, max = 15) String codigoISO) {
		ProvinciaResponse response = provinciaService.findByCodigoISO(codigoISO);
		if (response == null) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontro la provincia con codigo ISO " + codigoISO);
		}
		return response;
	}

	@GetMapping(value = "/findBy/pais/{codigoAlfa2}")
	public Iterable<ProvinciaResponse> findByPais(
			@PathVariable("codigoAlfa2") @NotBlank @Size(min = 2, max = 2) String codigoAlfa2) {
		List<ProvinciaResponse> list = provinciaService.findByPais(codigoAlfa2);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron provincias para el pais " + codigoAlfa2);
		}
		return list;
	}

	@GetMapping(value = "/findBy/nombre/{pais}/{nombre}")
	public Iterable<ProvinciaResponse> findByNombre(@PathVariable("pais") @NotBlank @Size(min = 2, max = 2) String pais,
			@PathVariable("nombre") @NotBlank @Size(min = 2, max = 100) String nombre) {
		List<ProvinciaResponse> list = provinciaService.findByNombre(pais, nombre);
		if (list == null || list.isEmpty()) {
			throw new ResponseStatusException(HttpStatus.NOT_FOUND,
					"No se encontraron provincias con nombre " + nombre + " para el pais " + pais);
		}
		return list;
	}
}
