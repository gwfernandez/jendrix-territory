package com.jendrix.territory.controller.rest;

import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jendrix.common.exception.ServiceException;
import com.jendrix.territory.controller.response.ProcessStatusResponse;
import com.jendrix.territory.process.sync.ContinenteSyncService;
import com.jendrix.territory.process.sync.LocalidadSyncService;
import com.jendrix.territory.process.sync.PaisSyncService;
import com.jendrix.territory.process.sync.PartidoSyncService;
import com.jendrix.territory.process.sync.ProvinciaSyncService;

@RestController
@RequestMapping("/sync")
@Validated
public class SyncController {

	@Autowired
	private ContinenteSyncService continenteSynService;

	@Autowired
	private PaisSyncService paisSyncService;

	@Autowired
	private ProvinciaSyncService provinciaSyncService;

	@Autowired
	private PartidoSyncService partidoSyncService;

	@Autowired
	private LocalidadSyncService localidadSyncService;

	@GetMapping(value = "/continentes")
	public ProcessStatusResponse syncContinentes() {
		try {
			continenteSynService.syncAll();
			return ProcessStatusResponse.success("Sincronizacion de continentes exitosa!!!");
		} catch (ServiceException e) {
			return ProcessStatusResponse.error(e.getMessage());
		}
	}

	@GetMapping(value = "/paises")
	public ProcessStatusResponse syncPaises() {
		try {
			paisSyncService.syncAll();
			return ProcessStatusResponse.success("Sincronizacion de paises exitosa!!!");
		} catch (ServiceException e) {
			return ProcessStatusResponse.error(e.getMessage());
		}
	}

	@GetMapping(value = "/paises/by")
	public ProcessStatusResponse syncPaisesBy(
			@RequestParam("continente") @Size(min = 2, max = 2) String codigoContinente) {
		try {
			paisSyncService.syncByContinente(codigoContinente);
			return ProcessStatusResponse
					.success("Sincronizacion de paises para el continente '" + codigoContinente + "' exitosa!!!");
		} catch (ServiceException e) {
			return ProcessStatusResponse.error(e.getMessage());
		}
	}

	@GetMapping(value = "/provincias/by")
	public ProcessStatusResponse syncProvinciasBy(
			@RequestParam(name = "continente", required = false, defaultValue = "") String continente,
			@RequestParam(name = "pais", required = false, defaultValue = "") String pais) {
		try {
			if (!continente.isEmpty()) {
				provinciaSyncService.syncByContinente(continente);
				return ProcessStatusResponse
						.success("Sincronizacion de provincias exitosa para el continente " + continente);
			} else if (!pais.isEmpty()) {
				provinciaSyncService.syncByPais(pais);
				return ProcessStatusResponse.success("Sincronizacion de provincias exitosa para el pais " + pais);
			} else {
				return ProcessStatusResponse.error("No especifico los parametros necesarios");
			}
		} catch (ServiceException e) {
			return ProcessStatusResponse.error(e.getMessage());
		}
	}

	@GetMapping(value = "/partidos/by")
	public ProcessStatusResponse syncPartidosBy(
			@RequestParam(name = "continente", required = false, defaultValue = "") String continente,
			@RequestParam(name = "pais", required = false, defaultValue = "") String pais,
			@RequestParam(name = "provincia", required = false, defaultValue = "") String provincia,
			@RequestParam(name = "provinciaId", required = false, defaultValue = "0") String provinciaID) {
		try {
			if (!continente.isEmpty()) {
				partidoSyncService.syncByContinente(continente);
				return ProcessStatusResponse
						.success("Sincronizacion de partidos exitosa para el continente " + continente);
			} else if (!pais.isEmpty()) {
				partidoSyncService.syncByPais(pais);
				return ProcessStatusResponse.success("Sincronizacion de partidos exitosa para el pais " + pais);
			} else if (!provincia.isEmpty()) {
				partidoSyncService.syncByProvincia(provincia);
				return ProcessStatusResponse
						.success("Sincronizacion de partidos exitosa para la provincia " + provincia);
			} else if (!provinciaID.isEmpty()) {
				partidoSyncService.syncByProvincia(Long.valueOf(provinciaID));
				return ProcessStatusResponse
						.success("Sincronizacion de partidos exitosa para la provincia ID: " + provinciaID);
			} else {
				return ProcessStatusResponse.error("No especifico los parametros necesarios");
			}
		} catch (ServiceException e) {
			return ProcessStatusResponse.error(e.getMessage());
		}
	}

	@GetMapping(value = "/localidades/by")
	public ProcessStatusResponse syncLocalidadesBy(
			@RequestParam(name = "continente", required = false, defaultValue = "") String continente,
			@RequestParam(name = "pais", required = false, defaultValue = "") String pais,
			@RequestParam(name = "provinciaId", required = false, defaultValue = "0") String provinciaID,
			@RequestParam(name = "partidoId", required = false, defaultValue = "0") String partidoID) {
		try {
			if (!continente.isEmpty()) {
				localidadSyncService.syncByContinente(continente);
				return ProcessStatusResponse
						.success("Sincronizacion de localidades exitosa para el continente " + continente);
			} else if (!pais.isEmpty()) {
				localidadSyncService.syncByPais(pais);
				return ProcessStatusResponse.success("Sincronizacion de localidades exitosa para el pais " + pais);
			} else if (!provinciaID.isEmpty()) {
				localidadSyncService.syncByProvincia(Long.valueOf(provinciaID));
				return ProcessStatusResponse
						.success("Sincronizacion de localidades exitosa para la provincia ID: " + provinciaID);
			} else if (!partidoID.isEmpty()) {
				localidadSyncService.syncByProvincia(partidoID);
				return ProcessStatusResponse
						.success("Sincronizacion de localidades exitosa para la provincia " + partidoID);
			} else {
				return ProcessStatusResponse.error("No especifico los parametros necesarios");
			}
		} catch (ServiceException e) {
			return ProcessStatusResponse.error(e.getMessage());
		}
	}

}
