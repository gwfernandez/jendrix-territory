package com.jendrix.territory.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.jendrix.common.embeddable.DateAudit;

@Entity
@Table(name = "continente")
@SuppressWarnings("serial")
public final class Continente implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "continente_id", unique = true)
	private Long id;

	@Column(name = "codigo", length = 2, unique = true)
	private String codigo;

	@Column(name = "nombre", length = 100, unique = true)
	private String nombre;

	@Column(name = "referencia", unique = true, nullable = false)
	private Long referencia;

	// TODO: column JSON con idioma {es: Europa, en: Europe}

	@Embedded
	private DateAudit dateAudit;

	public Continente() {
		this.id = 0l;
		this.dateAudit = new DateAudit();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getReferencia() {
		return referencia;
	}

	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}

	public DateAudit getDateAudit() {
		return dateAudit;
	}
}