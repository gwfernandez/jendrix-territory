package com.jendrix.territory.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jendrix.common.embeddable.DateAudit;

@Entity
@Table(name = "localidad")
@SuppressWarnings("serial")
public final class Localidad implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "localidad_id", unique = true)
	private Long id;

	@Column(length = 100, nullable = false)
	private String nombre;

	@Column(name = "referencia", unique = true, nullable = false)
	private Long referencia;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "partido_id", nullable = false)
	private Partido partido;

	@Embedded
	private DateAudit dateAudit;

	public Localidad() {
		this.id = 0l;
		this.dateAudit = new DateAudit();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getReferencia() {
		return referencia;
	}

	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}

	public Partido getPartido() {
		return partido;
	}

	public void setPartido(Partido partido) {
		this.partido = partido;
	}

	public DateAudit getDateAudit() {
		return dateAudit;
	}
}