package com.jendrix.territory.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jendrix.common.embeddable.DateAudit;

@Entity
@Table(name = "pais")
@SuppressWarnings("serial")
public final class Pais implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "pais_id", unique = true)
	private Long id;

	// codigo ISO 3166-1 alfa 2
	@Column(name = "codigo_alfa2", length = 2, unique = true, nullable = false)
	private String codigoAlfa2;

	// codigo ISO 3166-1 alfa 3
	@Column(name = "codigo_alfa3", length = 3, unique = true, nullable = false)
	private String codigoAlfa3;

	@Column(name = "nombre", length = 100, nullable = false)
	private String nombre;

	@Column(name = "bandera", nullable = true)
	private String bandera;

	@Column(name = "prefijo_telefonico")
	private Integer prefijoTelefonico;

	@Column(name = "referencia", unique = true, nullable = true)
	private Long referencia;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "continente_id", nullable = false)
	private Continente continente;

	// TODO JSON lenguaje

	// TODO JSON informacion

	@Embedded
	private DateAudit dateAudit;

	public Pais() {
		this.id = 0l;
		this.dateAudit = new DateAudit();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoAlfa2() {
		return codigoAlfa2;
	}

	public void setCodigoAlfa2(String codigoAlfa2) {
		this.codigoAlfa2 = codigoAlfa2;
	}

	public String getCodigoAlfa3() {
		return codigoAlfa3;
	}

	public void setCodigoAlfa3(String codigoAlfa3) {
		this.codigoAlfa3 = codigoAlfa3;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getBandera() {
		return bandera;
	}

	public void setBandera(String bandera) {
		this.bandera = bandera;
	}

	public Integer getPrefijoTelefonico() {
		return prefijoTelefonico;
	}

	public void setPrefijoTelefonico(Integer prefijoTelefonico) {
		this.prefijoTelefonico = prefijoTelefonico;
	}

	public Long getReferencia() {
		return referencia;
	}

	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}

	public Continente getContinente() {
		return continente;
	}

	public void setContinente(Continente continente) {
		this.continente = continente;
	}

	public DateAudit getDateAudit() {
		return dateAudit;
	}
}