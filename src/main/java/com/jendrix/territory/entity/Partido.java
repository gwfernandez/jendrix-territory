package com.jendrix.territory.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jendrix.common.embeddable.DateAudit;

@Entity
@Table(name = "partido")
@SuppressWarnings("serial")
public final class Partido implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "partido_id", unique = true)
	private Long id;

	@Column(name = "nombre", length = 100)
	private String nombre;

	@Column(name = "referencia", unique = true, nullable = false)
	private Long referencia;

	// TODO: column JSON con idioma {es: Europa, en: Europe}

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "provincia_id", nullable = false)
	private Provincia provincia;

	@Embedded
	private DateAudit dateAudit;

	public Partido() {
		this.id = 0l;
		this.dateAudit = new DateAudit();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getReferencia() {
		return referencia;
	}

	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}

	public DateAudit getDateAudit() {
		return dateAudit;
	}

}