package com.jendrix.territory.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.jendrix.common.embeddable.DateAudit;

@Entity
@Table(name = "provincia")
@SuppressWarnings("serial")
public final class Provincia implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "provincia_id", unique = true)
	private Long id;

	@Column(name = "nombre", length = 120, nullable = false)
	private String nombre;

	@Column(name = "codigo_iso", length = 25, nullable = false)
	private String codigoISO;

	@Column(name = "referencia", unique = true, nullable = false)
	private Long referencia;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "pais_id", nullable = false)
	private Pais pais;

	@Embedded
	private DateAudit dateAudit;

	public Provincia() {
		this.id = 0l;
		this.dateAudit = new DateAudit();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pais getPais() {
		return pais;
	}

	public void setPais(Pais pais) {
		this.pais = pais;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoISO() {
		return codigoISO;
	}

	public void setCodigoISO(String codigoISO) {
		this.codigoISO = codigoISO;
	}

	public Long getReferencia() {
		return referencia;
	}

	public void setReferencia(Long referencia) {
		this.referencia = referencia;
	}

	public DateAudit getDateAudit() {
		return dateAudit;
	}
}