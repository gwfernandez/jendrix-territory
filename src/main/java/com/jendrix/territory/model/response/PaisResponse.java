package com.jendrix.territory.model.response;

public class PaisResponse {

	private Long id;
	private String codigoAlfa2;
	private String codigoAlfa3;
	private String nombre;
	private String bandera;
	private ContinenteResponse continente;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCodigoAlfa2() {
		return codigoAlfa2;
	}

	public void setCodigoAlfa2(String codigoAlfa2) {
		this.codigoAlfa2 = codigoAlfa2;
	}

	public String getCodigoAlfa3() {
		return codigoAlfa3;
	}

	public void setCodigoAlfa3(String codigoAlfa3) {
		this.codigoAlfa3 = codigoAlfa3;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getBandera() {
		return bandera;
	}

	public void setBandera(String bandera) {
		this.bandera = bandera;
	}

	public ContinenteResponse getContinente() {
		return continente;
	}

	public void setContinente(ContinenteResponse continente) {
		this.continente = continente;
	}
}
