package com.jendrix.territory.model.response;

public class ProvinciaResponse {

	private Long id;
	private String nombre;
	private PaisResponse pais;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PaisResponse getPais() {
		return pais;
	}

	public void setPais(PaisResponse pais) {
		this.pais = pais;
	}
}
