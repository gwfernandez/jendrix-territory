package com.jendrix.territory.process.sync;

import com.jendrix.common.exception.ServiceException;

public interface ContinenteSyncService {

	public void syncAll() throws ServiceException;

}
