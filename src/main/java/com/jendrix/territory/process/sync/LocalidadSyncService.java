package com.jendrix.territory.process.sync;

import com.jendrix.common.exception.ServiceException;

public interface LocalidadSyncService {

	public void syncAll() throws ServiceException;

	public void syncByContinente(String codigo) throws ServiceException;

	public void syncByPais(String codigoAlfa2) throws ServiceException;

	public void syncByProvincia(String codigoISO) throws ServiceException;

	public void syncByProvincia(Long provinciaId) throws ServiceException;

	public void syncByPartido(Long partidoId) throws ServiceException;

}
