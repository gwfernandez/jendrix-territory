package com.jendrix.territory.process.sync;

import com.jendrix.common.exception.ServiceException;

public interface PaisSyncService {

	public void syncAll() throws ServiceException;

	public void syncByContinente(String codigo) throws ServiceException;

}