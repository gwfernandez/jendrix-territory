package com.jendrix.territory.process.sync;

import com.jendrix.common.exception.ServiceException;

public interface ProvinciaSyncService {

	public void syncAll() throws ServiceException;

	public void syncByContinente(String codigo) throws ServiceException;

	public void syncByPais(String codigoAlfa2) throws ServiceException;
}
