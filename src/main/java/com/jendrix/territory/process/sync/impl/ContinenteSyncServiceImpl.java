package com.jendrix.territory.process.sync.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.jendrix.common.exception.ServiceException;
import com.jendrix.territory.consumer.GeonamesConsumer;
import com.jendrix.territory.entity.Continente;
import com.jendrix.territory.process.sync.ContinenteSyncService;
import com.jendrix.territory.repository.ContinenteRepository;

@Service
public class ContinenteSyncServiceImpl implements ContinenteSyncService {

	private static final Log log = LogFactory.getLog(ContinenteSyncServiceImpl.class);
	private static final Long ID_INICIAL = 6295630l;

	@Autowired
	private ContinenteRepository continenteRepository;

	@Override
	public void syncAll() throws ServiceException {
		log.info("Inciando proceso de sincronizacion de continentes ...");
		try {
			Iterable<Continente> continentes = this.getContinentesRest();
			continentes.forEach(continente -> this.procesarContinente(continente));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error al sincronizar los continentes");
		}
		log.info("Fin proceso de sincronizacion de continentes!");
	}

	private Iterable<Continente> getContinentesRest() throws IOException {
		List<Continente> continentes = new ArrayList<>();
		HashMap<Long, String> codigoContinenteMap = ContinenteUtil.getContinentesGeonames();
		JsonNode geonameNode = GeonamesConsumer.request(ID_INICIAL);
		if (geonameNode != null) {
			Iterator<JsonNode> elements = geonameNode.elements();
			while (elements.hasNext()) {
				JsonNode node = elements.next();
				Continente continente = new Continente();
				continente.setNombre(node.path("name").asText());
				continente.setReferencia(node.path("geonameId").asLong());
				continente.setCodigo(codigoContinenteMap.get(continente.getReferencia()));
				continentes.add(continente);
			}
		}
		return continentes;
	}

	private void procesarContinente(Continente continente) {
		String msg = String.format("[%s-%s]", continente.getCodigo(), continente.getNombre());
		try {
			log.info("procesando continente " + msg);
			Continente original = this.continenteRepository.findByCodigoIgnoreCase(continente.getCodigo());
			if (original == null) {
				continente.getDateAudit().setFechaAlta(new Date());
				this.continenteRepository.save(continente);
			} else {
				original.setNombre(continente.getNombre());
				original.setReferencia(continente.getReferencia());
				original.getDateAudit().setFechaUltimaModificacion(new Date());
				this.continenteRepository.save(original);
			}
		} catch (Exception e) {
			log.error("Error al procesar continente " + msg);
			e.printStackTrace();
		}
	}
}
