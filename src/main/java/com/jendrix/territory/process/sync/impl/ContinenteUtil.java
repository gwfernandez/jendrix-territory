package com.jendrix.territory.process.sync.impl;

import java.util.HashMap;

public class ContinenteUtil {

	@SuppressWarnings("serial")
	public static HashMap<Long, String> getContinentesGeonames() {
		return new HashMap<Long, String>() {
			{
				put(6255146l, "AF");
				put(6255152l, "AN");
				put(6255147l, "AS");
				put(6255151l, "OC");
				put(6255148l, "EU");
				put(6255149l, "NA");
				put(6255150l, "SA");
			}
		};
	}

	@SuppressWarnings("serial")
	public static HashMap<String, String> getContinentesRescountries() {
		return new HashMap<String, String>() {
			{
				put("AF", "africa");
				put("NA", "americas");
				put("SA", "americas");
				put("AS", "asia");
				put("EU", "europe");
				put("OC", "oceania");
				put("AN", "");
			}
		};
	}

	public static void main(String[] args) {
		Iterable<String> continentesRescountries = ContinenteUtil.getContinentesRescountries().values();
		continentesRescountries.forEach(c -> System.out.println(c));
	}
}
