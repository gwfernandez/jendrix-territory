package com.jendrix.territory.process.sync.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.jendrix.common.exception.ServiceException;
import com.jendrix.territory.consumer.GeonamesConsumer;
import com.jendrix.territory.entity.Continente;
import com.jendrix.territory.entity.Localidad;
import com.jendrix.territory.entity.Pais;
import com.jendrix.territory.entity.Partido;
import com.jendrix.territory.entity.Provincia;
import com.jendrix.territory.process.sync.LocalidadSyncService;
import com.jendrix.territory.repository.ContinenteRepository;
import com.jendrix.territory.repository.LocalidadRepository;
import com.jendrix.territory.repository.PaisRepository;
import com.jendrix.territory.repository.PartidoRepository;
import com.jendrix.territory.repository.ProvinciaRepository;

@Service
public class LocalidadSyncServciceImpl implements LocalidadSyncService {

	// TODO: mejorar el manejo de excepciones dentro de los forEach

	private static final Log log = LogFactory.getLog(LocalidadSyncServciceImpl.class);

	@Autowired
	private ContinenteRepository continenteRepository;

	@Autowired
	private PaisRepository paisRepository;

	@Autowired
	private ProvinciaRepository provinciaRepository;

	@Autowired
	private PartidoRepository partidoRepository;

	@Autowired
	private LocalidadRepository localidadRepository;

	@Override
	public void syncAll() throws ServiceException {
		log.info("Inciando proceso de sincronizacion de localidades ...");
		try {
			Iterable<Continente> continentes = this.continenteRepository.findAll();
			continentes.forEach(continente -> {
				try {
					this.syncLocalidades(continente);
				} catch (Exception se) {
					log.error("Error al procesar las localidades del continente " + continente.getNombre());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error al sincronizar las localidades");
		}
		log.info("Fin proceso de sincronizacion de localidades!");
	}

	@Override
	public void syncByContinente(String codigoContinente) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de localidades para el continente " + codigoContinente + "...");
		Continente continente = this.continenteRepository.findByCodigoIgnoreCase(codigoContinente);
		if (continente != null) {
			syncLocalidades(continente);
		} else {
			throw new ServiceException("No existe el continente " + codigoContinente);
		}
		log.info("Fin proceso de sincronizacion de localidades para el continente " + codigoContinente);
	}

	@Override
	public void syncByPais(String codigoAlfa2) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de localidades para el pais " + codigoAlfa2 + "...");
		Pais pais = this.paisRepository.findByCodigoAlfa2IgnoreCase(codigoAlfa2);
		if (pais != null) {
			syncLocalidades(pais);
		} else {
			throw new ServiceException("No existe el pais " + codigoAlfa2);
		}
		log.info("Fin proceso de sincronizacion de localidades para el pais " + codigoAlfa2);
	}

	@Override
	public void syncByProvincia(String codigoISO) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de localidades para la provincia " + codigoISO + "...");
		Provincia provincia = this.provinciaRepository.findByCodigoISOIgnoreCase(codigoISO);
		if (provincia != null) {
			syncLocalidades(provincia);
		} else {
			throw new ServiceException("No existe la provincia " + codigoISO);
		}
		log.info("Fin proceso de sincronizacion de localidades para la provincia " + codigoISO);
	}

	@Override
	public void syncByProvincia(Long provinciaId) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de localidades para la provincia ID " + provinciaId + "...");
		Provincia provincia = this.provinciaRepository.findById(provinciaId).orElse(null);
		if (provincia != null) {
			syncLocalidades(provincia);
		} else {
			throw new ServiceException("No existe la provincia con ID " + provinciaId);
		}
		log.info("Fin proceso de sincronizacion de localidades para la provincia ID " + provinciaId);
	}

	@Override
	public void syncByPartido(Long partidoId) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de localidades para el partido ID " + partidoId + "...");
		Partido partido = this.partidoRepository.findById(partidoId).orElse(null);
		if (partido != null) {
			syncLocalidades(partido);
		} else {
			throw new ServiceException("No existe el partido con ID " + partidoId);
		}
		log.info("Fin proceso de sincronizacion de localidades para el partido ID " + partidoId);
	}

	private void syncLocalidades(Continente continente) throws ServiceException {
		String msg = String.format("[cont=%s]", continente.getCodigo());
		log.info("Sincronizando localidades " + msg);
		List<Pais> paises = this.paisRepository.findByContinenteId(continente.getId());
		if (!paises.isEmpty()) {
			paises.forEach(pa -> {
				try {
					this.syncLocalidades(pa);
				} catch (Exception e) {
					log.error("Error procesando los localidades del pais " + pa.getCodigoAlfa2());
				}
			});
		} else {
			log.warn("El " + msg + " no tiene paises!!");
		}
	}

	private void syncLocalidades(Pais pais) throws ServiceException {
		String msg = String.format("[cont=%s][pais=%s]", pais.getContinente().getCodigo(), pais.getCodigoAlfa2());
		log.info("Sincronizando localidades " + msg);
		Iterable<Provincia> provincias = this.provinciaRepository.findByPaisId(pais.getId());
		if (provincias != null) {
			provincias.forEach(provincia -> {
				try {
					this.syncLocalidades(provincia);
				} catch (Exception e) {
					log.error("Error procesando los localidades de la provincia " + provincia.getReferencia() + ", "
							+ provincia.getNombre());
				}
			});
		} else {
			log.warn("El " + msg + " no tiene provincias!!");
		}
	}

	private void syncLocalidades(Provincia provincia) throws ServiceException {
		String msg = String.format("[cont=%s][pais=%s][prov=%s]", provincia.getPais().getContinente().getCodigo(),
				provincia.getPais().getCodigoAlfa2(), provincia.getNombre());
		log.info("Sincronizando localidades " + msg);
		Iterable<Partido> partidos = this.partidoRepository.findByProvinciaId(provincia.getId());
		if (partidos != null) {
			partidos.forEach(partido -> this.syncLocalidades(partido));
		} else {
			log.warn("El " + msg + " no tiene partidos!!");
		}
	}

	private void syncLocalidades(Partido partido) {
		esperarProximaInvocacion(10);
		String msg = String.format("[cont=%s][pais=%s][prov=%s][part=%s]",
				partido.getProvincia().getPais().getContinente().getCodigo(),
				partido.getProvincia().getPais().getCodigoAlfa2(), partido.getProvincia().getNombre(),
				partido.getNombre());
		try {
			log.info("Sincronizando localidades " + msg);
			List<Localidad> localidades = this.getLocalidadesFromGeonames(partido);
			if (!localidades.isEmpty()) {
				localidades.forEach(l -> this.procesarLocalidadFromGeonames(l));
			} else {
				log.warn("El partido " + msg + " no tiene localidades!");
			}
		} catch (IOException e) {
			log.error("Error al consultar las localidades " + msg);
			e.printStackTrace();
		}
	}

	private List<Localidad> getLocalidadesFromGeonames(Partido partido) throws IOException {
		List<Localidad> localidades = new ArrayList<>();
		JsonNode geonameNode = GeonamesConsumer.request(partido.getReferencia());
		if (geonameNode != null) {
			Iterator<JsonNode> elements = geonameNode.elements();
			while (elements.hasNext()) {
				JsonNode node = elements.next();
				Localidad localidad = new Localidad();
				localidad.setNombre(node.path("name").asText());
				localidad.setPartido(partido);
				localidad.setReferencia(node.path("geonameId").asLong());
				localidades.add(localidad);
			}
		} else {
			log.warn("El partido " + partido.getReferencia() + " - " + partido.getNombre() + " no tiene localidades!");
		}
		return localidades;
	}

	private void procesarLocalidadFromGeonames(Localidad localidad) {
		String msg = String.format("[cont=%s][pais=%s][prov=%s][part=%s][loc=%s]",
				localidad.getPartido().getProvincia().getPais().getContinente().getCodigo(),
				localidad.getPartido().getProvincia().getPais().getCodigoAlfa2(),
				localidad.getPartido().getProvincia().getReferencia(), localidad.getPartido().getReferencia(),
				localidad.getNombre());
		try {
			log.info("procesando " + msg);
			Localidad original = this.localidadRepository.findByReferencia(localidad.getReferencia());
			if (original == null) {
				localidad.getDateAudit().setFechaAlta(new Date());
				this.localidadRepository.save(localidad);
			} else {
				original.setNombre(localidad.getNombre());
				original.setPartido(localidad.getPartido());
				original.getDateAudit().setFechaUltimaModificacion(new Date());
				this.localidadRepository.save(original);
			}
		} catch (Exception e) {
			log.error("Error al procesar " + msg);
			e.printStackTrace();
		}
	}

	private void esperarProximaInvocacion(int segundosMaximo) {
		int sleep = (int) ((Math.random() * segundosMaximo) + 1) * 1000;
		try {
			System.out.println("Proxima sincronizacion en " + sleep + "ms");
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			log.error("Error realizando la espera de invocacion");
		}
	}

}
