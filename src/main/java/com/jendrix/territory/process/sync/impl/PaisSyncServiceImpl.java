package com.jendrix.territory.process.sync.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.jendrix.common.exception.ServiceException;
import com.jendrix.territory.consumer.GeonamesConsumer;
import com.jendrix.territory.consumer.RestcountriesConsumer;
import com.jendrix.territory.entity.Continente;
import com.jendrix.territory.entity.Pais;
import com.jendrix.territory.process.sync.PaisSyncService;
import com.jendrix.territory.repository.ContinenteRepository;
import com.jendrix.territory.repository.PaisRepository;

@Service
public class PaisSyncServiceImpl implements PaisSyncService {

	private static final Log log = LogFactory.getLog(ContinenteSyncServiceImpl.class);

	@Autowired
	private ContinenteRepository continenteRepository;

	@Autowired
	private PaisRepository paisRepository;

	@Override
	public void syncAll() throws ServiceException {

		log.info("Inciando proceso de sincronizacion de paises ...");
		try {
			log.info("sincronizando paises desde GEONAMES ...");
			Iterable<Continente> continentes = this.continenteRepository.findAll();
			continentes.forEach(c -> {
				try {
					esperarProximaInvocacion(10);
					Iterable<Pais> paises = this.getPaisesFromGeonames(c);
					paises.forEach(p -> this.procesarPaisFromGeonames(p));
				} catch (Exception se) {
					log.error("Error al procesar los paises del continente " + c.getNombre());
				}
			});

			log.info("sincronizando paises desde RESTCOUNTRIES ...");
			HashMap<String, String> continentesRescountries = ContinenteUtil.getContinentesRescountries();
			continentesRescountries.forEach((codigoContinente, codigoRescountries) -> {
				if (!codigoRescountries.isEmpty()) {
					try {
						esperarProximaInvocacion(10);
						Iterable<Pais> paises = this.getPaisesRestCountries(codigoRescountries);
						paises.forEach(p -> this.procesarPaisFromRestCountries(p));
					} catch (Exception se) {
						log.error("Error al procesar los paises del continente " + codigoContinente);
					}
				} else {
					log.warn("No hay paises para el continente " + codigoContinente);
				}
			});

		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error al sincronizar los paises");
		}
		log.info("Fin proceso de sincronizacion de paises!");
	}

	@Override
	public void syncByContinente(String codigoContinente) throws ServiceException {

		log.info("Inciando proceso de sincronizacion de paises por continente ...");
		Continente c = this.continenteRepository.findByCodigoIgnoreCase(codigoContinente);
		if (c != null) {
			try {
				log.info("sincronizando paises desde GEONAMES ...");
				Iterable<Pais> paises = this.getPaisesFromGeonames(c);
				paises.forEach(p -> this.procesarPaisFromGeonames(p));

				log.info("sincronizando paises desde RESTCOUNTRIES ...");
				String continenteCodigo = ContinenteUtil.getContinentesRescountries().get(c.getCodigo());
				if (!continenteCodigo.isEmpty()) {
					Iterable<Pais> list = this.getPaisesRestCountries(continenteCodigo);
					list.forEach(p -> {
						p.setContinente(c);
						this.procesarPaisFromRestCountries(p);
					});
				} else {
					log.warn("No hay paises para el continente " + continenteCodigo);
				}
			} catch (Exception se) {
				log.error("Error al procesar los paises del continente " + c.getNombre());
				throw new ServiceException("Error al procesar los paises del continente " + c.getNombre());
			}
		} else {
			throw new ServiceException("No existe el continente " + codigoContinente);
		}
	}

	private Iterable<Pais> getPaisesFromGeonames(Continente continente) throws IOException {
		List<Pais> paises = new ArrayList<>();
		JsonNode geonameNode = GeonamesConsumer.request(continente.getReferencia());
		if (geonameNode != null) {
			Iterator<JsonNode> elements = geonameNode.elements();
			while (elements.hasNext()) {
				JsonNode node = elements.next();
				Pais pais = new Pais();
				pais.setNombre(node.path("countryName").asText());
				pais.setCodigoAlfa2(node.path("countryCode").asText());
				pais.setCodigoAlfa3(node.path("countryCode").asText());
				pais.setContinente(continente);
				pais.setReferencia(node.path("geonameId").asLong());
				paises.add(pais);
			}
		} else {
			log.warn("No se encontraron paises para el continente: " + continente.getCodigo());
		}
		return paises;
	}

	private void procesarPaisFromGeonames(Pais pais) {
		String msg = String.format("[continente=%s][pais=%s-%s]", pais.getContinente().getCodigo(),
				pais.getCodigoAlfa2(), pais.getNombre());
		try {
			log.info("procesando " + msg);
			Pais original = this.paisRepository.findByCodigoAlfa2IgnoreCase(pais.getCodigoAlfa2());
			if (original == null) {
				pais.getDateAudit().setFechaAlta(new Date());
				this.paisRepository.save(pais);
			} else {
				original.setNombre(pais.getNombre());
				original.setCodigoAlfa2(pais.getCodigoAlfa2());
				original.setCodigoAlfa3(pais.getCodigoAlfa3());
				original.setContinente(pais.getContinente());
				original.setBandera(pais.getBandera());
				original.getDateAudit().setFechaUltimaModificacion(new Date());
				this.paisRepository.save(original);
			}
		} catch (Exception e) {
			log.error("Error al procesar: " + msg);
			e.printStackTrace();
		}
	}

	private Iterable<Pais> getPaisesRestCountries(String continente) throws IOException {
		List<Pais> paises = new ArrayList<>();
		JsonNode root = RestcountriesConsumer.request(continente);
		if (root != null) {
			Iterator<JsonNode> elements = root.elements();
			while (elements.hasNext()) {
				JsonNode node = elements.next();
				Pais pais = new Pais();
				pais.setCodigoAlfa2(node.path("alpha2Code").asText());
				pais.setCodigoAlfa3(node.path("alpha3Code").asText());
				try {
					Iterator<JsonNode> it = node.path("callingCodes").elements();
					if (node.path("callingCodes").elements().hasNext()) {
						pais.setPrefijoTelefonico(it.next().asInt());
					}
					pais.setBandera(node.path("flag").asText());
					// JsonNode translations = node.path("translations");
					// JsonNode es = translations.findValue("es");
				} catch (Exception e) {
					log.warn("No se encontraron algunos datos del pais " + pais.getCodigoAlfa2());
				}
				paises.add(pais);
			}
		} else {
			log.warn("No se encontraron paises para el continente: " + continente);
		}
		return paises;
	}

	private void procesarPaisFromRestCountries(Pais pais) {
		String msg = String.format("[%s][%s]", pais.getContinente().getCodigo(), pais.getCodigoAlfa2());
		try {
			log.info("procesando " + msg);

			Pais original = this.paisRepository.findByCodigoAlfa2IgnoreCase(pais.getCodigoAlfa2());
			if (original != null) {
				original.setCodigoAlfa3(pais.getCodigoAlfa3());
				original.setBandera(pais.getBandera());
				original.setPrefijoTelefonico(pais.getPrefijoTelefonico());
				original.getDateAudit().setFechaUltimaModificacion(new Date());
				this.paisRepository.save(original);
			} else {
				log.warn("El pais " + pais.getCodigoAlfa2() + " no fue dado de alta en geonames");
			}
		} catch (Exception e) {
			log.error("Error al procesar: " + msg);
			e.printStackTrace();
		}
	}

	private void esperarProximaInvocacion(int segundosMaximo) {
		int sleep = (int) ((Math.random() * segundosMaximo) + 1) * 1000;
		try {
			System.out.println("Proxima sincronizacion en " + sleep + "ms");
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			log.error("Error realizando la espera de invocacion");
		}
	}

}
