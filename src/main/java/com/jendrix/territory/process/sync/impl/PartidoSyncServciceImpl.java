package com.jendrix.territory.process.sync.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.jendrix.common.exception.ServiceException;
import com.jendrix.territory.consumer.GeonamesConsumer;
import com.jendrix.territory.entity.Continente;
import com.jendrix.territory.entity.Pais;
import com.jendrix.territory.entity.Partido;
import com.jendrix.territory.entity.Provincia;
import com.jendrix.territory.process.sync.PartidoSyncService;
import com.jendrix.territory.repository.ContinenteRepository;
import com.jendrix.territory.repository.PaisRepository;
import com.jendrix.territory.repository.PartidoRepository;
import com.jendrix.territory.repository.ProvinciaRepository;

@Service
public class PartidoSyncServciceImpl implements PartidoSyncService {

	// TODO: mejorar el manejo de excepciones dentro de los forEach

	private static final Log log = LogFactory.getLog(PartidoSyncServciceImpl.class);

	@Autowired
	private ContinenteRepository continenteRepository;

	@Autowired
	private PaisRepository paisRepository;

	@Autowired
	private ProvinciaRepository provinciaRepository;

	@Autowired
	private PartidoRepository partidoRepository;

	@Override
	public void syncAll() throws ServiceException {
		log.info("Inciando proceso de sincronizacion de partidos ...");
		try {
			Iterable<Continente> continentes = this.continenteRepository.findAll();
			continentes.forEach(continente -> {
				try {
					this.syncPartidos(continente);
				} catch (Exception se) {
					log.error("Error al procesar las partidos del continente " + continente.getNombre());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error al sincronizar las partidos");
		}
		log.info("Fin proceso de sincronizacion de partidos!");
	}

	@Override
	public void syncByContinente(String codigoContinente) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de partidos para el continente " + codigoContinente + "...");
		Continente continente = this.continenteRepository.findByCodigoIgnoreCase(codigoContinente);
		if (continente != null) {
			syncPartidos(continente);
		} else {
			throw new ServiceException("No existe el continente " + codigoContinente);
		}
		log.info("Fin proceso de sincronizacion de partidos para el continente " + codigoContinente);
	}

	@Override
	public void syncByPais(String codigoAlfa2) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de partidos para el pais " + codigoAlfa2 + "...");
		Pais pais = this.paisRepository.findByCodigoAlfa2IgnoreCase(codigoAlfa2);
		if (pais != null) {
			syncPartidos(pais);
		} else {
			throw new ServiceException("No existe el pais " + codigoAlfa2);
		}
		log.info("Fin proceso de sincronizacion de partidos para el pais " + codigoAlfa2);
	}

	@Override
	public void syncByProvincia(String codigoISO) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de partidos para la provincia " + codigoISO + "...");
		Provincia provincia = this.provinciaRepository.findByCodigoISOIgnoreCase(codigoISO);
		if (provincia != null) {
			syncPartidos(provincia);
		} else {
			throw new ServiceException("No existe la provincia " + codigoISO);
		}
		log.info("Fin proceso de sincronizacion de partidos para la provincia " + codigoISO);
	}

	@Override
	public void syncByProvincia(Long provinciaId) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de partidos para la provincia ID " + provinciaId + "...");
		Provincia provincia = this.provinciaRepository.findById(provinciaId).orElse(null);
		if (provincia != null) {
			syncPartidos(provincia);
		} else {
			throw new ServiceException("No existe la provincia con ID " + provinciaId);
		}
		log.info("Fin proceso de sincronizacion de partidos para la provincia ID " + provinciaId);
	}

	private void syncPartidos(Continente continente) throws ServiceException {
		String msg = String.format("[continente=%s]", continente.getCodigo());
		log.info("Sincronizando partidos " + msg);
		List<Pais> paises = this.paisRepository.findByContinenteId(continente.getId());
		if (!paises.isEmpty()) {
			paises.forEach(pa -> {
				try {
					this.syncPartidos(pa);
				} catch (Exception e) {
					log.error("Error procesando los partidos del pais " + pa.getCodigoAlfa2());
				}
			});
		} else {
			log.warn("El " + msg + " no tiene paises!!");
		}
	}

	private void syncPartidos(Pais pais) throws ServiceException {
		String msg = String.format("[continente=%s][pais=%s]", pais.getContinente().getCodigo(), pais.getCodigoAlfa2());
		log.info("Sincronizando partidos " + msg);
		Iterable<Provincia> provincias = this.provinciaRepository.findByPaisId(pais.getId());
		if (provincias != null) {
			provincias.forEach(pr -> this.syncPartidos(pr));
		} else {
			log.warn("El " + msg + " no tiene provincias!!");
		}
	}

	private void syncPartidos(Provincia provincia) {
		esperarProximaInvocacion(10);
		String msg = String.format("[cn=%s][pa=%s][pr=%s]", provincia.getPais().getContinente().getCodigo(),
				provincia.getPais().getCodigoAlfa2(), provincia.getNombre());
		try {
			log.info("Sincronizando partidos " + msg);
			List<Partido> partidos = this.getPartidosFromGeonames(provincia);
			if (!partidos.isEmpty()) {
				partidos.forEach(p -> this.procesarPartidoFromGeonames(p));
			} else {
				log.warn("La provincia " + msg + " no tiene partidos!");
			}
		} catch (IOException e) {
			log.error("Error al consultar los partidos " + msg);
			e.printStackTrace();
		}
	}

	private List<Partido> getPartidosFromGeonames(Provincia provincia) throws IOException {
		List<Partido> partidos = new ArrayList<>();
		JsonNode geonameNode = GeonamesConsumer.request(provincia.getReferencia());
		if (geonameNode != null) {
			Iterator<JsonNode> elements = geonameNode.elements();
			while (elements.hasNext()) {
				JsonNode node = elements.next();
				Partido partido = new Partido();
				partido.setNombre(node.path("name").asText());
				partido.setProvincia(provincia);
				partido.setReferencia(node.path("geonameId").asLong());
				partidos.add(partido);
			}
		} else {
			log.warn("La provincia " + provincia.getReferencia() + " - " + provincia.getNombre()
					+ " no tiene partidos!");
		}
		return partidos;
	}

	private void procesarPartidoFromGeonames(Partido partido) {
		String msg = String.format("[con=%s][pai=%s][pro=%s][par=%s]",
				partido.getProvincia().getPais().getContinente().getCodigo(),
				partido.getProvincia().getPais().getCodigoAlfa2(), partido.getProvincia().getReferencia(),
				partido.getNombre());
		try {
			log.info("procesando " + msg);
			Partido original = this.partidoRepository.findByReferencia(partido.getReferencia());
			if (original == null) {
				partido.getDateAudit().setFechaAlta(new Date());
				this.partidoRepository.save(partido);
			} else {
				original.setNombre(partido.getNombre());
				original.setProvincia(partido.getProvincia());
				original.getDateAudit().setFechaUltimaModificacion(new Date());
				this.partidoRepository.save(original);
			}
		} catch (Exception e) {
			log.error("Error al procesar " + msg);
			e.printStackTrace();
		}
	}

	private void esperarProximaInvocacion(int segundosMaximo) {
		int sleep = (int) ((Math.random() * segundosMaximo) + 1) * 1000;
		try {
			System.out.println("Proxima sincronizacion en " + sleep + "ms");
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			log.error("Error realizando la espera de invocacion");
		}
	}

}
