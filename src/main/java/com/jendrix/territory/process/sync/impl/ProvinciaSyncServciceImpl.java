package com.jendrix.territory.process.sync.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.JsonNode;
import com.jendrix.common.exception.ServiceException;
import com.jendrix.territory.consumer.GeonamesConsumer;
import com.jendrix.territory.entity.Continente;
import com.jendrix.territory.entity.Pais;
import com.jendrix.territory.entity.Provincia;
import com.jendrix.territory.process.sync.ProvinciaSyncService;
import com.jendrix.territory.repository.ContinenteRepository;
import com.jendrix.territory.repository.PaisRepository;
import com.jendrix.territory.repository.ProvinciaRepository;

@Service
public class ProvinciaSyncServciceImpl implements ProvinciaSyncService {

	// TODO: mejorar el manejo de excepciones dentro de los forEach

	private static final Log log = LogFactory.getLog(ProvinciaSyncServciceImpl.class);

	@Autowired
	private ContinenteRepository continenteRepository;

	@Autowired
	private PaisRepository paisRepository;

	@Autowired
	private ProvinciaRepository provinciaRepository;

	@Override
	public void syncAll() throws ServiceException {
		log.info("Inciando proceso de sincronizacion de provincias ...");
		try {
			Iterable<Continente> continentes = this.continenteRepository.findAll();
			continentes.forEach(continente -> {
				try {
					this.syncProvincias(continente);
				} catch (Exception se) {
					log.error("Error al procesar las provincias del continente " + continente.getNombre());
				}
			});
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error al sincronizar las provincias");
		}
		log.info("Fin proceso de sincronizacion de provincias!");
	}

	@Override
	public void syncByContinente(String codigoContinente) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de provincias para el continente " + codigoContinente + "...");
		Continente continente = this.continenteRepository.findByCodigoIgnoreCase(codigoContinente);
		if (continente != null) {
			syncProvincias(continente);
		} else {
			throw new ServiceException("No existe el continente " + codigoContinente);
		}
		log.info("Fin proceso de sincronizacion de provincias para el continente " + codigoContinente);
	}

	@Override
	public void syncByPais(String codigoAlfa2) throws ServiceException {
		log.info("Inciando proceso de sincronizacion de provincias para el pais " + codigoAlfa2 + "...");
		Pais pais = this.paisRepository.findByCodigoAlfa2IgnoreCase(codigoAlfa2);
		if (pais != null) {
			syncProvincias(pais);
		} else {
			throw new ServiceException("No existe el pais " + codigoAlfa2);
		}
		log.info("Fin proceso de sincronizacion de provincias para el pais " + codigoAlfa2);
	}

	private void syncProvincias(Continente continente) throws ServiceException {
		String msg = String.format("[continente=%s]", continente.getCodigo());
		log.info("Sincronizando provincias " + msg);
		List<Pais> paises = this.paisRepository.findByContinenteId(continente.getId());
		if (!paises.isEmpty()) {
			paises.forEach(p -> this.syncProvincias(p));
		} else {
			log.warn("El " + msg + " no tiene paises!!");
		}
	}

	private void syncProvincias(Pais pais) {
		esperarProximaInvocacion(10);
		String msg = String.format("[continente=%s][pais=%s]", pais.getContinente().getCodigo(), pais.getCodigoAlfa2());
		try {
			log.info("Sincronizando provincias " + msg);
			List<Provincia> provincias = this.getProvinciasFromGeonames(pais);
			if (!provincias.isEmpty()) {
				provincias.forEach(p -> this.procesarProvinciaFromGeonames(p));
			} else {
				log.warn("El pais " + msg + " no tiene provincias!");
			}
		} catch (IOException e) {
			log.error("Error al consultar las provincias " + msg);
			e.printStackTrace();
		}
	}

	private List<Provincia> getProvinciasFromGeonames(Pais pais) throws IOException {
		List<Provincia> provincias = new ArrayList<>();
		JsonNode geonameNode = GeonamesConsumer.request(pais.getReferencia());
		if (geonameNode != null) {
			Iterator<JsonNode> elements = geonameNode.elements();
			while (elements.hasNext()) {
				JsonNode node = elements.next();
				Provincia provincia = new Provincia();
				provincia.setNombre(node.path("name").asText());
				provincia.setPais(pais);
				provincia.setReferencia(node.path("geonameId").asLong());
				String codigoISO = null;
				try {
					JsonNode adminCodes1 = node.path("adminCodes1");
					codigoISO = pais.getCodigoAlfa2() + "-" + adminCodes1.findValue("ISO3166_2").asText();
					provincia.setCodigoISO(codigoISO);
				} catch (Exception e) {
					try {
						JsonNode adminCode1 = node.path("adminCode1");
						codigoISO = pais.getCodigoAlfa2() + "-" + adminCode1.asText();
					} catch (Exception ex) {
					}

					if (codigoISO.isEmpty()) {
						log.warn("La provincia no tiene codigo ISO. referencia: " + provincia.getReferencia());
						provincia.setCodigoISO(provincia.getReferencia().toString());
					} else {
						provincia.setCodigoISO(codigoISO);
					}
				}
				provincias.add(provincia);
			}
		} else {
			log.warn("El pais" + pais.getCodigoAlfa2() + " no tiene provincias!");
		}
		return provincias;
	}

	private void procesarProvinciaFromGeonames(Provincia provincia) {
		String msg = String.format("[continente=%s][pais=%s][provincia=%s]",
				provincia.getPais().getContinente().getCodigo(), provincia.getPais().getCodigoAlfa2(),
				provincia.getNombre());
		try {
			log.info("procesando " + msg);
			Provincia original = this.provinciaRepository.findByReferencia(provincia.getReferencia());
			if (original == null) {
				provincia.getDateAudit().setFechaAlta(new Date());
				this.provinciaRepository.save(provincia);
			} else {
				original.setNombre(provincia.getNombre());
				original.setPais(provincia.getPais());
				original.setCodigoISO(provincia.getCodigoISO());
				original.getDateAudit().setFechaUltimaModificacion(new Date());
				this.provinciaRepository.save(original);
			}
		} catch (Exception e) {
			log.error("Error al procesar " + msg);
			e.printStackTrace();
		}
	}

	private void esperarProximaInvocacion(int segundosMaximo) {
		int sleep = (int) ((Math.random() * segundosMaximo) + 1) * 1000;
		try {
			System.out.println("Proxima sincronizacion en " + sleep + "ms");
			Thread.sleep(sleep);
		} catch (InterruptedException e) {
			log.error("Error realizando la espera de invocacion");
		}
	}

}
