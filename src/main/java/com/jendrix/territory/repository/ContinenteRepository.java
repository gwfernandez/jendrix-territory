package com.jendrix.territory.repository;

import org.springframework.data.repository.CrudRepository;

import com.jendrix.territory.entity.Continente;

public interface ContinenteRepository extends CrudRepository<Continente, Long> {

	public Continente findByCodigoIgnoreCase(String codigo);

	public Iterable<Continente> findByNombreContainingIgnoreCase(String nombre);

}
