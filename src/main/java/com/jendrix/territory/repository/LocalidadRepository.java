package com.jendrix.territory.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jendrix.territory.entity.Localidad;

public interface LocalidadRepository extends CrudRepository<Localidad, Long> {

	public Localidad findByReferencia(Long referencia);

	@Query("select l from Localidad l where l.partido.id = ?1 and upper(l.nombre) like CONCAT('%',upper(?2),'%') and l.dateAudit.fechaBaja is null")
	public Iterable<Localidad> findByNombre(Long partidoId, String nombre);

	@Query("select l from Localidad l where l.partido.id = ?1 and l.dateAudit.fechaBaja is null")
	public Iterable<Localidad> findByPartidoId(Long partidoId);

	@Query("select l from Localidad l where l.partido.provincia.id = ?1 and l.dateAudit.fechaBaja is null")
	public Iterable<Localidad> findByProvinciaId(Long provinciaId);

	@Query("select l from Localidad l where l.partido.provincia.codigoISO = upper(?1) and l.dateAudit.fechaBaja is null")
	public Iterable<Localidad> findByProvincia(String codigoISO);

}
