package com.jendrix.territory.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jendrix.territory.entity.Pais;

public interface PaisRepository extends CrudRepository<Pais, Long> {

	public Pais findByCodigoAlfa2IgnoreCase(String codigo);

	public Pais findByCodigoAlfa3IgnoreCase(String codigo);

	public Iterable<Pais> findByNombreContainingIgnoreCase(String nombre);

	public Iterable<Pais> findByNombreStartingWithIgnoreCase(String letra);

	@Query("select p from Pais p where lower(p.continente.codigo) = lower(?1) and p.dateAudit.fechaBaja is null")
	public Iterable<Pais> findByContinente(String continente);

	@Query("select p from Pais p where p.continente.id = ?1 and p.dateAudit.fechaBaja is null")
	public List<Pais> findByContinenteId(Long continenteId);

}
