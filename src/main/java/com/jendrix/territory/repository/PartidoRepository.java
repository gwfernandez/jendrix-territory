package com.jendrix.territory.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jendrix.territory.entity.Partido;

public interface PartidoRepository extends CrudRepository<Partido, Long> {

	public Partido findByReferencia(Long referencia);

	@Query("select p from Partido p where p.provincia.id = ?1 and upper(p.nombre) like CONCAT('%',upper(?2),'%') and p.dateAudit.fechaBaja is null")
	public Iterable<Partido> findByNombre(Long provinciaId, String nombre);

	@Query("select p from Partido p where p.provincia.codigoISO = upper(?1) and upper(p.nombre) like CONCAT('%',upper(?2),'%') and p.dateAudit.fechaBaja is null")
	public Iterable<Partido> findByNombre(String codigoISOProvincia, String nombre);

	@Query("select p from Partido p where p.provincia.codigoISO = upper(?1) and p.dateAudit.fechaBaja is null")
	public Iterable<Partido> findByProvincia(String codigoISO);

	@Query("select p from Partido p where p.provincia.id = ?1 and p.dateAudit.fechaBaja is null")
	public Iterable<Partido> findByProvinciaId(Long provinciaId);

}
