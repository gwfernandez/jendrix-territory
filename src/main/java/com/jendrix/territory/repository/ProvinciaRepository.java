package com.jendrix.territory.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.jendrix.territory.entity.Provincia;

public interface ProvinciaRepository extends CrudRepository<Provincia, Long> {

	public Optional<Provincia> findById(Long id);

	public Provincia findByReferencia(Long referencia);

	public Provincia findByCodigoISOIgnoreCase(String codigoISO);

	@Query("select p from Provincia p where p.pais.codigoAlfa2 = upper(?1) and upper(p.nombre) like CONCAT('%',upper(?2),'%') and p.dateAudit.fechaBaja is null")
	public Iterable<Provincia> findByNombre(String pais, String nombre);

	@Query("select p from Provincia p where p.pais.codigoAlfa2 = upper(?1) and p.dateAudit.fechaBaja is null")
	public Iterable<Provincia> findByPais(String codigoAlfa2);

	@Query("select p from Provincia p where p.pais.id = ?1 and p.dateAudit.fechaBaja is null")
	public Iterable<Provincia> findByPaisId(Long paisId);

}
