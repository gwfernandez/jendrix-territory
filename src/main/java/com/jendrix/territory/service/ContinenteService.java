package com.jendrix.territory.service;

import java.util.List;

import com.jendrix.territory.model.response.ContinenteResponse;

public interface ContinenteService {

	public Iterable<ContinenteResponse> findAll();

	public ContinenteResponse findByCodigo(String codigo);

	public ContinenteResponse findById(Long id);

	public List<ContinenteResponse> findByNombre(String nombre);
}
