package com.jendrix.territory.service;

import com.jendrix.territory.model.response.LocalidadResponse;

public interface LocalidadService {

	public LocalidadResponse findById(Long id);

	public Iterable<LocalidadResponse> findByNombre(Long partidoId, String nombre);

	public Iterable<LocalidadResponse> findByPartidoId(Long partidoId);

}
