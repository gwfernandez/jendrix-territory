package com.jendrix.territory.service;

import java.util.List;

import com.jendrix.territory.model.response.PaisResponse;

public interface PaisService {

	public PaisResponse findById(Long id);

	public PaisResponse findByCodigoAlfa2(String codigo);

	public PaisResponse findByCodigoAlfa3(String codigo);

	public List<PaisResponse> findByNombre(String nombre);

	public List<PaisResponse> findByNombreStartingWith(String letra);

	public List<PaisResponse> findByContinente(String continente);

}
