package com.jendrix.territory.service;

import java.util.List;

import com.jendrix.territory.model.response.PartidoResponse;

public interface PartidoService {

	public PartidoResponse findById(Long id);

	public List<PartidoResponse> findByNombre(Long provinciaId, String nombre);

	public List<PartidoResponse> findByProvincia(String codigoISO);

	public List<PartidoResponse> findByProvinciaId(Long provinciaId);
}
