package com.jendrix.territory.service;

import java.util.List;

import com.jendrix.territory.model.response.ProvinciaResponse;

public interface ProvinciaService {

	public ProvinciaResponse findById(Long id);

	public List<ProvinciaResponse> findByNombre(String paisCodigoAlfa2, String nombre);

	public List<ProvinciaResponse> findByPais(String codigoAlfa2);

	public ProvinciaResponse findByCodigoISO(String codigoISO);
}
