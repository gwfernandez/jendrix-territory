package com.jendrix.territory.service.impl;

import java.util.List;

import org.dozer.Mapper;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.mapper.Mappeable;
import com.jendrix.territory.entity.Continente;
import com.jendrix.territory.model.response.ContinenteResponse;
import com.jendrix.territory.repository.ContinenteRepository;
import com.jendrix.territory.service.ContinenteService;

@Service("continenteServiceImpl")
public class ContinenteServiceImpl implements ContinenteService {

	@Autowired
	private ContinenteRepository continenteRepository;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	protected Mappeable<Continente, ContinenteResponse> getMapper() {
		return c -> this.dozerMapper.map(c, ContinenteResponse.class);
	}

	@Override
	public List<ContinenteResponse> findAll() {
		try {
			return this.getMapper().mapList(this.continenteRepository.findAll());
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findAll. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public ContinenteResponse findById(Long id) {
		try {
			ContinenteResponse response = null;
			Continente continente = this.continenteRepository.findById(id).orElse(null);
			if (continente != null) {
				response = this.getMapper().map(continente);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findById. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public ContinenteResponse findByCodigo(String codigo) {
		try {
			ContinenteResponse response = null;
			Continente continente = this.continenteRepository.findByCodigoIgnoreCase(codigo);
			if (continente != null) {
				response = this.getMapper().map(continente);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByCodigo. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<ContinenteResponse> findByNombre(String nombre) {
		try {
			return getMapper().mapList(this.continenteRepository.findByNombreContainingIgnoreCase(nombre));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByNombre. Mensaje: " + e.getMessage());
		}
	}
}
