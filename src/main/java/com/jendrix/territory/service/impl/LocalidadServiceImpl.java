package com.jendrix.territory.service.impl;

import java.util.List;

import org.dozer.Mapper;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.mapper.Mappeable;
import com.jendrix.territory.entity.Localidad;
import com.jendrix.territory.model.response.LocalidadResponse;
import com.jendrix.territory.repository.LocalidadRepository;
import com.jendrix.territory.service.LocalidadService;

@Service("localidadServiceImpl")
public class LocalidadServiceImpl implements LocalidadService {

	@Autowired
	private LocalidadRepository localidadRepository;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	protected Mappeable<Localidad, LocalidadResponse> getMapper() {
		return c -> this.dozerMapper.map(c, LocalidadResponse.class);
	}

	@Override
	public LocalidadResponse findById(Long id) {
		try {
			LocalidadResponse response = null;
			Localidad partido = this.localidadRepository.findById(id).orElse(null);
			if (partido != null) {
				response = this.getMapper().map(partido);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findById. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<LocalidadResponse> findByNombre(Long partidoId, String nombre) {
		try {
			return this.getMapper().mapList(this.localidadRepository.findByNombre(partidoId, nombre));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByNombre. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public Iterable<LocalidadResponse> findByPartidoId(Long partidoId) {
		try {
			return this.getMapper().mapList(this.localidadRepository.findByPartidoId(partidoId));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByPartido. Mensaje: " + e.getMessage());
		}
	}

}
