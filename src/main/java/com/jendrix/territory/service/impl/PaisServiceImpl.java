package com.jendrix.territory.service.impl;

import java.util.List;

import org.dozer.Mapper;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.mapper.Mappeable;
import com.jendrix.territory.entity.Pais;
import com.jendrix.territory.model.response.PaisResponse;
import com.jendrix.territory.repository.PaisRepository;
import com.jendrix.territory.service.PaisService;

@Service("paisServiceImpl")
public class PaisServiceImpl implements PaisService {

	@Autowired
	private PaisRepository paisRepository;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	protected Mappeable<Pais, PaisResponse> getMapper() {
		return c -> this.dozerMapper.map(c, PaisResponse.class);
	}

	@Override
	public PaisResponse findById(Long id) {
		try {
			PaisResponse response = null;
			Pais pais = this.paisRepository.findById(id).orElse(null);
			if (pais != null) {
				response = this.getMapper().map(pais);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findById. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public PaisResponse findByCodigoAlfa2(String codigo) {
		try {
			PaisResponse response = null;
			Pais pais = this.paisRepository.findByCodigoAlfa2IgnoreCase(codigo);
			if (pais != null) {
				response = this.getMapper().map(pais);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByCodigoAlfa2. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public PaisResponse findByCodigoAlfa3(String codigo) {
		try {
			PaisResponse response = null;
			Pais pais = this.paisRepository.findByCodigoAlfa3IgnoreCase(codigo);
			if (pais != null) {
				response = this.getMapper().map(pais);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByCodigoAlfa3. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<PaisResponse> findByNombre(String nombre) {
		try {
			return this.getMapper().mapList(this.paisRepository.findByNombreContainingIgnoreCase(nombre));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByNombre. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<PaisResponse> findByNombreStartingWith(String letra) {
		try {
			return this.getMapper().mapList(this.paisRepository.findByNombreStartingWithIgnoreCase(letra));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByNombreStartingWith. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<PaisResponse> findByContinente(String continente) {
		try {
			return this.getMapper().mapList(this.paisRepository.findByContinente(continente));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByContinente. Mensaje: " + e.getMessage());
		}
	}
}
