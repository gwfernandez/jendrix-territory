package com.jendrix.territory.service.impl;

import java.util.List;

import org.dozer.Mapper;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.mapper.Mappeable;
import com.jendrix.territory.entity.Partido;
import com.jendrix.territory.model.response.PartidoResponse;
import com.jendrix.territory.repository.PartidoRepository;
import com.jendrix.territory.service.PartidoService;

@Service("partidoServiceImpl")
public class PartidoServiceImpl implements PartidoService {

	@Autowired
	private PartidoRepository partidoRepository;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	protected Mappeable<Partido, PartidoResponse> getMapper() {
		return c -> this.dozerMapper.map(c, PartidoResponse.class);
	}

	@Override
	public PartidoResponse findById(Long id) {
		try {
			PartidoResponse response = null;
			Partido partido = this.partidoRepository.findById(id).orElse(null);
			if (partido != null) {
				response = this.getMapper().map(partido);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findById. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<PartidoResponse> findByNombre(Long provinciaId, String nombre) {
		try {
			return this.getMapper().mapList(this.partidoRepository.findByNombre(provinciaId, nombre));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByNombre. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<PartidoResponse> findByProvincia(String codigoISO) {
		try {
			return this.getMapper().mapList(this.partidoRepository.findByProvincia(codigoISO));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByProvincia. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<PartidoResponse> findByProvinciaId(Long provinciaId) {
		try {
			return this.getMapper().mapList(this.partidoRepository.findByProvinciaId(provinciaId));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByProvinciaId. Mensaje: " + e.getMessage());
		}
	}

}
