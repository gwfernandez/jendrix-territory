package com.jendrix.territory.service.impl;

import java.util.List;

import org.dozer.Mapper;
import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jendrix.common.component.mapper.Mappeable;
import com.jendrix.territory.entity.Provincia;
import com.jendrix.territory.model.response.ProvinciaResponse;
import com.jendrix.territory.repository.ProvinciaRepository;
import com.jendrix.territory.service.ProvinciaService;

@Service("provinciaServiceImpl")
public class ProvinciaServiceImpl implements ProvinciaService {

	@Autowired
	private ProvinciaRepository provinciaRepository;

	@Autowired
	private Mapper dozerMapper;

	@Autowired
	protected Mappeable<Provincia, ProvinciaResponse> getMapper() {
		return c -> this.dozerMapper.map(c, ProvinciaResponse.class);
	}

	@Override
	public ProvinciaResponse findById(Long id) {
		try {
			ProvinciaResponse response = null;
			Provincia provincia = this.provinciaRepository.findById(id).orElse(null);
			if (provincia != null) {
				response = this.getMapper().map(provincia);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findById. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public ProvinciaResponse findByCodigoISO(String codigoISO) {
		try {
			ProvinciaResponse response = null;
			Provincia provincia = this.provinciaRepository.findByCodigoISOIgnoreCase(codigoISO);
			if (provincia != null) {
				response = this.getMapper().map(provincia);
			}
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByCodigoISO. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<ProvinciaResponse> findByNombre(String paisCodigoAlfa2, String nombre) {
		try {
			return this.getMapper().mapList(this.provinciaRepository.findByNombre(paisCodigoAlfa2, nombre));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByNombre. Mensaje: " + e.getMessage());
		}
	}

	@Override
	public List<ProvinciaResponse> findByPais(String codigoAlfa2) {
		try {
			return this.getMapper().mapList(this.provinciaRepository.findByPais(codigoAlfa2));
		} catch (Exception e) {
			e.printStackTrace();
			throw new ServiceException("Error en findByPais. Mensaje: " + e.getMessage());
		}
	}

}
